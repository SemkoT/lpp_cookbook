package xstmpx.lppcookbook.recipe_list;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.request.RequestOptions;
import com.mikepenz.fastadapter.FastAdapter;

import java.util.List;

import jp.wasabeef.glide.transformations.CropSquareTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import xstmpx.lppcookbook.R;
import xstmpx.lppcookbook.core.Const;
import xstmpx.lppcookbook.core.model.Recipe;
import xstmpx.lppcookbook.recipe_detail.DetailActivityController;

/**
 * Created by xstmp on 3/25/2018.
 */

public class RecipeViewHolder extends FastAdapter.ViewHolder<Recipe> {
    TextView tvTitle;
    TextView tvAuthor;
    ImageView ivImage;
    TextView tvRating;

    public RecipeViewHolder(View view) {
        super(view);
        tvTitle = (TextView) view.findViewById(R.id.recipe_item_name);
        tvAuthor = (TextView) view.findViewById(R.id.recipe_item_author);
        tvRating = (TextView) view.findViewById(R.id.recipe_item_rating);
        ivImage = (ImageView) view.findViewById(R.id.recipe_item_image);
    }

    @Override
    public void bindView(final Recipe item, List<Object> payloads) {
        tvTitle.setText(item.getTitle());
        tvAuthor.setText(item.getPublisher());
        tvRating.setText(Integer.toString((int) Double.parseDouble(item.getSocialRank())));
        int cornerRadiusInPixels = (int) itemView.getResources().getDimension(R.dimen.recipe_item_corner_radius);
        MultiTransformation multi = new MultiTransformation(
                new CropSquareTransformation(),
                new RoundedCornersTransformation(cornerRadiusInPixels, 0, RoundedCornersTransformation.CornerType.TOP));
        try {
            Glide.with(itemView.getContext())
                    .load(item.getImageUrl()).apply(RequestOptions.bitmapTransform(multi)).into(ivImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.getRecipeId() != null && !item.getRecipeId().isEmpty()) {
                    Intent openDetails = new Intent(itemView.getContext(), DetailActivityController.class);
                    openDetails.putExtra(Const.EXTRA_RECIPE_ID, item.getRecipeId());
                    itemView.getContext().startActivity(openDetails);
                }
            }
        });
    }

    @Override
    public void unbindView(Recipe item) {
        tvTitle.setText(null);
    }
}