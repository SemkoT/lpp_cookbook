package xstmpx.lppcookbook.recipe_list;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import xstmpx.lppcookbook.R;
import xstmpx.lppcookbook.Utils.Dialogs;
import xstmpx.lppcookbook.core.communication.Food2ForkFacade;
import xstmpx.lppcookbook.core.model.RecipeListMeta;


public class RecipeListActivity extends AppCompatActivity implements RecipeListFragmentController.Listener {
    RecipeListFragmentController recipeListFragmentController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_main);
        recipeListFragmentController = new RecipeListFragmentController();
        recipeListFragmentController.setListener(this);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.tmp_fragment_holder, recipeListFragmentController);
        transaction.commit();

    }

    private void getTopRatedRecipes(int page) {
        Food2ForkFacade.getInstance().getTopRatedRecipes(new Food2ForkFacade.TopRatedRecipesListener() {
            @Override
            public void onTopRatedRecipes(RecipeListMeta recipeListMeta) {
                recipeListFragmentController.loadRecipes(recipeListMeta);
            }

            @Override
            public void onError() {
                handleError();
            }
        }, page);
    }

    private void handleError() {
        recipeListFragmentController.hideProgress();
        Dialogs.showErrorDialog(RecipeListActivity.this);
    }

    private void searchRecipes(String searchPhrase, int page) {
        Food2ForkFacade.getInstance().searchRecipes(new Food2ForkFacade.SearchListener() {
            @Override
            public void onSearchResult(RecipeListMeta recipeListMeta) {
                recipeListFragmentController.loadRecipes(recipeListMeta);
            }

            @Override
            public void onError() {
                handleError();
            }
        }, searchPhrase, page);
    }

    @Override
    public void onFragmentReady() {
        getTopRatedRecipes(0);
    }

    @Override
    public void onSearchClicked(String searchPhrase) {
        searchRecipes(searchPhrase, 0);
    }

    @Override
    public void onLoadMore(int page) {
        getTopRatedRecipes(page);
    }

    @Override
    public void onLoadMoreSearch(String searchPhrase, int page) {
        searchRecipes(searchPhrase, page);
    }
}
