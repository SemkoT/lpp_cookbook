package xstmpx.lppcookbook.recipe_list;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mikepenz.fastadapter.FastAdapter;
import com.mikepenz.fastadapter.IAdapter;
import com.mikepenz.fastadapter.adapters.ItemAdapter;

import java.util.ArrayList;
import java.util.List;

import xstmpx.lppcookbook.R;
import xstmpx.lppcookbook.core.model.RecipeListMeta;


/**
 * Created by Semko on 3/25/2018.
 */

public class RecipeListFragmentView {
    private View fragmentView;
    private Listener listener;
    private RecyclerView rvRecipeList;
    private EditText etSearch;
    private TextView tvEmpty;
    private ImageView ivCancelSearch;
    private ProgressBar progressBar;
    private Context context;
    private GridLayoutManager rvLayoutManager;
    private Handler handler = new Handler(Looper.getMainLooper());
    private ItemAdapter itemAdapter;

    public RecipeListFragmentView(Context context, Listener listener, View fragmentView) {
        this.fragmentView = fragmentView;
        this.listener = listener;
        this.context = context;
        initViews();
    }

    private void initList() {
        int numberOfColumns = calculateNoOfColumns();
        if (numberOfColumns > 0) {
            rvRecipeList.setHasFixedSize(true);
            rvLayoutManager = new GridLayoutManager(context, numberOfColumns);
            rvRecipeList.setLayoutManager(rvLayoutManager);

            itemAdapter = new ItemAdapter();
            List<IAdapter> adapters = new ArrayList<>();
            adapters.add(itemAdapter);
            final FastAdapter fastAdapter = FastAdapter.with(adapters);
            rvRecipeList.setAdapter(fastAdapter);
            rvRecipeList.addOnScrollListener(new EndlessRecyclerViewScrollListener(rvLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount) {
                    showProgress();
                    if (etSearch.getText().toString().isEmpty()) {
                        listener.onLoadMore(page + 1);
                    } else {
                        listener.onLoadMoreSearch(etSearch.getText().toString(), page + 1);
                    }
                }
            });
        }
    }

    public void displayRecipes(final RecipeListMeta recipeListMeta) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (recipeListMeta.getCount() > 0) {
                    itemAdapter.add(recipeListMeta.getRecipes());
                }
                if (itemAdapter.getAdapterItemCount() < 1) {
                    tvEmpty.setVisibility(View.VISIBLE);
                } else {
                    tvEmpty.setVisibility(View.GONE);
                }
                hideProgress();
            }
        });
    }

    public void showProgress() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    public void hideProgress() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void initViews() {
        rvRecipeList = (RecyclerView) fragmentView.findViewById(R.id.recipe_list_recycler_view);
        progressBar = (ProgressBar) fragmentView.findViewById(R.id.recipe_list_progress_bar);
        ivCancelSearch = (ImageView) fragmentView.findViewById(R.id.recipe_list_cancel_search);
        ivCancelSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                etSearch.setText("");
                listener.onSearchClicked("");
                showProgress();
                clearList();
            }
        });
        etSearch = (EditText) fragmentView.findViewById(R.id.recipe_list_search);
        tvEmpty = (TextView) fragmentView.findViewById(R.id.recipe_list_empty);
        tvEmpty.setVisibility(View.GONE);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (etSearch.getText().toString().isEmpty()) {
                    ivCancelSearch.setVisibility(View.GONE);
                } else {
                    ivCancelSearch.setVisibility(View.VISIBLE);
                }
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    listener.onSearchClicked(etSearch.getText().toString());
                    itemAdapter.clear();
                    hideKeyboard();
                    showProgress();
                    return true;
                }
                return false;
            }
        });
        fragmentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                handler.post(new Runnable() {
                    public void run() {
                        if (rvLayoutManager == null) {
                            initList();
                            listener.onViewReady();
                        }
                    }
                });
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(fragmentView.getWindowToken(), 0);
    }

    private int calculateNoOfColumns() {
        int imageWidth = (int) context.getResources().getDimension(R.dimen.recipe_item_image_width);
        return (int) (fragmentView.getMeasuredWidth() / imageWidth);
    }

    public void clearList() {
        if (itemAdapter != null) {
            itemAdapter.clear();
        }
    }

    interface Listener {
        void onViewReady();

        void onSearchClicked(String searchPhrase);

        void onLoadMore(int page);

        void onLoadMoreSearch(String searchPhrase, int page);
    }
}
