package xstmpx.lppcookbook.recipe_list;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xstmpx.lppcookbook.R;
import xstmpx.lppcookbook.core.model.RecipeListMeta;

public class RecipeListFragmentController extends Fragment implements RecipeListFragmentView.Listener {
    private RecipeListFragmentView view;
    private Listener listener;

    public RecipeListFragmentController() {
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_recipe_list, container, false);
        view = new RecipeListFragmentView(getContext(), this, fragmentView);
        return fragmentView;
    }

    @Override
    public void onViewReady() {
        if (listener != null) {
            listener.onFragmentReady();
        }
    }

    @Override
    public void onSearchClicked(String searchPhrase) {
        if (listener != null) {
            listener.onSearchClicked(searchPhrase);
        }
    }

    @Override
    public void onLoadMore(int page) {
        listener.onLoadMore(page);
    }

    @Override
    public void onLoadMoreSearch(String searchPhrase, int page) {
        listener.onLoadMoreSearch(searchPhrase, page);
    }

    public void loadRecipes(RecipeListMeta recipeListMeta) {
        view.displayRecipes(recipeListMeta);
    }

    public void clearList() {
        view.clearList();
    }

    public void hideProgress() {
        view.hideProgress();
    }

    public interface Listener {
        void onFragmentReady();

        void onSearchClicked(String searchPhrase);

        void onLoadMore(int page);

        void onLoadMoreSearch(String searchPhrase, int page);
    }
}
