package xstmpx.lppcookbook.recipe_detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import xstmpx.lppcookbook.R;
import xstmpx.lppcookbook.core.model.Recipe;

public class DetailActivityView {
    private Listener listener;
    private View view;
    private TextView tvTitle;
    private TextView tvTitle2;
    private TextView tvAuthor;
    private TextView tvRating;
    private TextView tvIngredients;
    private ProgressBar progressBar;
    private ImageView ivImage;
    private LinearLayout llActionBar;
    private AppCompatButton btnDirections;
    private Handler handler = new Handler(Looper.getMainLooper());

    public DetailActivityView(View view, Listener listener) {
        this.listener = listener;
        this.view = view;
        initViews();
    }

    private void initViews() {
        tvTitle = (TextView) view.findViewById(R.id.detail_title);
        tvTitle2 = (TextView) view.findViewById(R.id.detail_title2);
        tvAuthor = (TextView) view.findViewById(R.id.detail_author);
        tvRating = (TextView) view.findViewById(R.id.detail_rating);
        tvIngredients = (TextView) view.findViewById(R.id.detail_ingredients);
        ivImage = (ImageView) view.findViewById(R.id.detail_image);
        llActionBar = (LinearLayout) view.findViewById(R.id.detail_action_bar);
        btnDirections = view.findViewById(R.id.recipe_directions_btn);
        llActionBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBack();
            }
        });
        progressBar = (ProgressBar) view.findViewById(R.id.detail_progress_bar);
    }

    public void loadData(final Recipe recipe) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                btnDirections.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(recipe.getSourceUrl()));
                        view.getContext().startActivity(browserIntent);
                    }
                });
                btnDirections.setText(String.format(view.getContext().getString(R.string.btn_directions), recipe.getPublisher()));
                progressBar.setVisibility(View.GONE);
                tvTitle.setText(recipe.getTitle());
                tvRating.setText(String.format(view.getContext().getString(R.string.rating), ((int) Double.parseDouble(recipe.getSocialRank()))));
                tvRating.setVisibility(View.VISIBLE);
                tvTitle2.setText(recipe.getTitle());
                btnDirections.setVisibility(View.VISIBLE);
                tvAuthor.setText(String.format(view.getContext().getString(R.string.author_prefix), recipe.getPublisher()));
                String ingredients = view.getContext().getString(R.string.ingredients);
                for (String ingredient : recipe.getIngredients()) {
                    ingredients = ingredients.concat("\n-").concat(ingredient);
                }
                tvIngredients.setText(ingredients);
                Glide.with(view.getContext()).load(recipe.getImageUrl()).into(ivImage);
            }
        });
    }

    public interface Listener {
        void onBack();
    }
}
