package xstmpx.lppcookbook.recipe_detail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import xstmpx.lppcookbook.R;
import xstmpx.lppcookbook.core.Const;
import xstmpx.lppcookbook.core.communication.Food2ForkFacade;
import xstmpx.lppcookbook.core.model.RecipeMeta;

public class DetailActivityController extends AppCompatActivity implements Food2ForkFacade.GetRecipeListener, DetailActivityView.Listener {
    private DetailActivityView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        view = new DetailActivityView(findViewById(android.R.id.content), this);
        String recipeId = getIntent().getStringExtra(Const.EXTRA_RECIPE_ID);
        Food2ForkFacade.getInstance().getRecipe(this, recipeId);
    }

    @Override
    public void onError() {
        finish();
    }

    @Override
    public void onRecipe(RecipeMeta recipeMeta) {
        view.loadData(recipeMeta.getRecipe());
    }

    @Override
    public void onBack() {
        finish();
    }
}
