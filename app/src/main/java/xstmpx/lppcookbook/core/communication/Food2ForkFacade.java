package xstmpx.lppcookbook.core.communication;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import xstmpx.lppcookbook.core.Const;
import xstmpx.lppcookbook.core.model.RecipeListMeta;
import xstmpx.lppcookbook.core.model.RecipeMeta;

/**
 * Created by Semko on 3/24/2018.
 */

public class Food2ForkFacade {
    private static Food2ForkFacade instance;
    private Retrofit retrofit;
    private Food2ForkService food2ForkService;

    Food2ForkFacade() {
        retrofit = new Retrofit.Builder().baseUrl(Const.FOOD_2_FORK_API_URL).addConverterFactory(GsonConverterFactory.create()).build();
        food2ForkService = retrofit.create(Food2ForkService.class);
    }

    public static Food2ForkFacade getInstance() {
        if (instance == null) {
            instance = new Food2ForkFacade();
        }
        return instance;
    }

    public void getTopRatedRecipes(final TopRatedRecipesListener listener, int page) {
        Call<RecipeListMeta> search = food2ForkService.search(Const.FOOD_2_FORK_API_KEY, Integer.toString(page));
        search.enqueue(new Callback<RecipeListMeta>() {
            @Override
            public void onResponse(Call<RecipeListMeta> call, Response<RecipeListMeta> response) {
                if (response.isSuccessful()) {
                    listener.onTopRatedRecipes(response.body());
                } else {
                    listener.onError();
                }
            }

            @Override
            public void onFailure(Call<RecipeListMeta> call, Throwable t) {
                listener.onError();
            }
        });
    }

    public void getRecipe(final GetRecipeListener listener, String recipeId) {
        Call<RecipeMeta> getRecipe = food2ForkService.getRecipe(Const.FOOD_2_FORK_API_KEY, recipeId);
        getRecipe.enqueue(new Callback<RecipeMeta>() {
            @Override
            public void onResponse(Call<RecipeMeta> call, Response<RecipeMeta> response) {
                if (response.isSuccessful()) {
                    listener.onRecipe(response.body());
                } else {
                    listener.onError();
                }
            }

            @Override
            public void onFailure(Call<RecipeMeta> call, Throwable t) {
                listener.onError();
            }
        });
    }

    public void searchRecipes(final SearchListener searchListener, String searchPhrase, int page) {
        Call<RecipeListMeta> search = food2ForkService.searchPhrase(Const.FOOD_2_FORK_API_KEY, searchPhrase, Integer.toString(page));
        search.enqueue(new Callback<RecipeListMeta>() {
            @Override
            public void onResponse(Call<RecipeListMeta> call, Response<RecipeListMeta> response) {
                if (response.isSuccessful()) {
                    searchListener.onSearchResult(response.body());
                } else {
                    searchListener.onError();
                }
            }

            @Override
            public void onFailure(Call<RecipeListMeta> call, Throwable t) {
                searchListener.onError();
            }
        });
    }

    public interface IError {
        void onError();
    }

    public interface GetRecipeListener extends IError {
        void onRecipe(RecipeMeta recipe);
    }

    public interface SearchListener extends IError {
        void onSearchResult(RecipeListMeta recipeListMeta);
    }

    public interface TopRatedRecipesListener extends IError {
        void onTopRatedRecipes(RecipeListMeta recipeListMeta);
    }
}
