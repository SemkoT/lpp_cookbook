package xstmpx.lppcookbook.core.communication;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import xstmpx.lppcookbook.core.model.RecipeListMeta;
import xstmpx.lppcookbook.core.model.RecipeMeta;

/**
 * Created by Semko on 3/24/2018.
 */

public interface Food2ForkService {
    @POST("search")
    @FormUrlEncoded
    Call<RecipeListMeta> search(@Field("key") String apiKey);

    @POST("search")
    @FormUrlEncoded
    Call<RecipeListMeta> search(@Field("key") String apiKey, @Field("page") String page);

    @POST("search")
    @FormUrlEncoded
    Call<RecipeListMeta> searchPhrase(@Field("key") String apiKey, @Field("q") String searchPhrase);

    @POST("search")
    @FormUrlEncoded
    Call<RecipeListMeta> searchPhrase(@Field("key") String apiKey, @Field("q") String searchPhrase, @Field("page") String page);

    @POST("get")
    @FormUrlEncoded
    Call<RecipeMeta> getRecipe(@Field("key") String apiKey, @Field("rId") String recipeId);

}
