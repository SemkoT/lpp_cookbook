package xstmpx.lppcookbook.core.model;

import android.support.annotation.NonNull;
import android.view.View;

import com.google.gson.annotations.SerializedName;
import com.mikepenz.fastadapter.items.AbstractItem;

import xstmpx.lppcookbook.R;
import xstmpx.lppcookbook.recipe_list.RecipeViewHolder;

/**
 * Created by Semko on 3/24/2018.
 */

public class Recipe extends AbstractItem<Recipe, RecipeViewHolder> {
    private String publisher;
    @SerializedName("f2f_url")
    private String f2fUrl;
    private String title;
    @SerializedName("source_url")
    private String sourceUrl;
    @SerializedName("recipe_id")
    private String recipeId;
    @SerializedName("image_url")
    private String imageUrl;
    @SerializedName("social_rank")
    private String socialRank;
    @SerializedName("publisher_url")
    private String publisherUrl;
    private String page;
    private String[] ingredients;

    public String getPublisher() {
        return publisher;
    }

    public String getF2fUrl() {
        return f2fUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getSocialRank() {
        return socialRank;
    }

    public String getPublisherUrl() {
        return publisherUrl;
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.item_recipe;
    }

    @NonNull
    @Override
    public RecipeViewHolder getViewHolder(View v) {
        return new RecipeViewHolder(v);
    }

    public String getPage() {
        return page;
    }

    public String[] getIngredients() {
        return ingredients;
    }
}
