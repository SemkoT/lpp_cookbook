package xstmpx.lppcookbook.core.model;

import java.util.List;

/**
 * Created by Semko on 3/24/2018.
 */

public class RecipeListMeta {
    private int count;
    private List<Recipe> recipes;

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public int getCount() {
        return count;
    }
}
